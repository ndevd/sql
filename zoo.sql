create database zoo_neodev;
use zoo_neodev;

create table if not exists habitat(
nombre varchar(20) not null primary key,
clima varchar(20),
vegetacion varchar(20)
);

create table if not exists itinerario(
codigo integer not null primary key;
duracion integer,
longitud integer,
visitantes integer
);

create table if not exists especie(
nombre_com varchar(20) not null primary key,
nombre_cient varchar(20),
descrip varchar(40),
foto varchar(20)
);

create table if not exists recorre(
nombrehabitat VARCHAR(20) NOT NULL PRIMARY KEY,
codigoitinerario INTEGER,
FOREIGN KEY(nombrehabitat) REFERENCES habitat(nombre),
FOREIGN KEY(codigoitinerario) REFERENCES itinerario(codigo)
);

create table if not exists vive_en(
nombrehabitat VARCHAR(20) not null,
nombre_comespecie VARCHAR(20) not null,
indice INTEGER,
PRIMARY KEY(nombrehabitab, nombre_comespecie),
FOREIGN KEY(nombrehabitab) REFERENCES habitat(nombre),
FOREIGN KEY(nombre_comespecie) REFERENCES especie(nombre_com)
);